// Чому для роботи з input не рекомендується використовувати клавіатуру?

// На різних розкладках до однієї і тієї ж кнопки можуть бути прив'язані різні символи.
//  краще використовувати  event.code  його перевага полягає в тому, що його значення завжди залишається незмінним,
//  будучи прив'язаним до фізичного розташування клавіші, навіть якщо користувач змінює мову.
//  Так що гарячі клавіші, що використовують цю властивість,
//   працюватимуть навіть у разі перемикання мови.
//

let btn = document.querySelectorAll(".btn");

document.addEventListener("keydown", function (event) {
  for (let key of btn) {
    key.style.backgroundColor = "#000000";
  }

  key = document.querySelector(".btn." + event.code);
  if (key) {
    key.style.backgroundColor = "blue";
  }
});
